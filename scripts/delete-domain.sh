#!/usr/bin/env bash
for ((i=1;i<=$#;i++))
do
    if [ ${!i} = "--site" ];
    then ((i++))
        site=${!i};
    fi
done

# Remove directory
sudo rm -rf /var/www/sites/${site}

# Drop MySQL database
mysqladmin -uroot -proot drop ${site} -f

# Disable website
sudo a2dissite ${site}
sudo service apache2 reload

# Clean up
sudo rm -f /etc/apache2/sites-available/${site}.conf
sudo rm -f /home/vagrant/certs/${site}.key
sudo rm -f /home/vagrant/certs/${site}.csr
sudo rm -f /home/vagrant/certs/${site}.ext
sudo rm -f /home/vagrant/certs/${site}

# Restart web server
sudo service apache2 restart
