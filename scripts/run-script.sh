#!/usr/bin/env bash
for ((i=1;i<=$#;i++))
do
    if [ ${!i} = "--directory" ];
    then ((i++))
        directory=${!i};

    elif [ ${!i} = "--script" ];
    then ((i++))
        script=${!i};

    elif [ ${!i} = "--args" ];
    then ((i++))
        args=${!i};
    fi
done

# Move to proper directory
cd ${directory}

# start server
bash ${script} ${args}
