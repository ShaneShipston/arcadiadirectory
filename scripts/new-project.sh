#!/usr/bin/env bash
for ((i=1;i<=$#;i++))
do
    if [ ${!i} = "--theme" ];
    then ((i++))
        theme=${!i};

    elif [ ${!i} = "--pages" ];
    then ((i++))
        pages=${!i};

    elif [ ${!i} = "--title" ];
    then ((i++))
        title=${!i};

    elif [ ${!i} = "--domain" ];
    then ((i++))
        domain=${!i};

    elif [ ${!i} = "--tld" ];
    then ((i++))
        tld=${!i};

    elif [ ${!i} = "--directory" ];
    then ((i++))
        directory=${!i};

    elif [ ${!i} = "--repo" ];
    then ((i++))
        repo=${!i};

    elif [ ${!i} = "--commit" ];
    then ((i++))
        commit=${!i};
    fi
done

# Move to server root
cd ${directory}

# Defaults
if [ -z ${domain+x} ]; then
    domain="arcadia";
fi

if [ -z ${theme+x} ]; then
    theme=${domain};
fi

if [ -z ${repo+x} ]; then
    repo="shoutmedia/${domain}";
fi

if [ -z ${tld+x} ]; then
    tld="vm";
fi

if [ -z ${commit+x} ]; then
    commit="Cloned base theme";
fi

# Clone config
cp scripts/config.json config/${domain}.json

# Update Theme
if [ ${theme+x} ]; then
    sed -i'.backup' "s/\"theme\": \"arcadia\"/\"theme\": \"${theme}\"/g" config/${domain}.json
fi

# Update folder
sed -i'.backup' "s/\"folder\": \"public\"/\"folder\": \"${domain}\"/g" config/${domain}.json

# Update database name
sed -i'.backup' "s/\"name\": \"arcadiabox\"/\"name\": \"${domain}\"/g" config/${domain}.json

# Update Pages
if [ ${pages+x} ]; then
    IFS=','; pagelist=($pages)

    pagefeed='';

    for element in "${pagelist[@]}"
    do
        pagefeed+="\"$(echo -e "${element}" | sed -e 's/^[[:space:]]*//' -e 's/[[:space:]]*$//')\""

        if [ "${pagelist[${#pagelist[@]}-1]}" != "${element}" ]; then
            pagefeed+=","
        fi
    done

    sed -i".backup" "s/\"pages\": \[\]/\"pages\": \[${pagefeed}\]/g" config/${domain}.json
fi

# Update Domain
sed -i'.backup' "s/\"url\": \"arcadia.local\"/\"url\": \"${domain}.${tld}\"/g" config/${domain}.json

# Update Title
if [ ${title+x} ]; then
    sed -i'.backup' "s/\"title\": \"WPDev\"/\"title\": \"${title}\"/g" config/${domain}.json
fi

# Install WP
ssh vagrant@192.168.33.10 -oStrictHostKeyChecking=no "bash /var/www/scripts/install-wp.sh --config ${domain}"

# Clean up
rm config/${domain}.json.backup

# Switch to theme
cd sites/${domain}/wp-content/themes/${theme}

# Update gulpfile
sed -i'.backup' "s/\"url\": \"arcadia.local\"/\"url\": \"${domain}.${tld}\"/g" gulpfile.js

# Clean up
rm gulpfile.js.backup

# Remove history
rm -rf .git

# Create new repo
git init
git remote add origin git@bitbucket.org:${repo}.git
git add .
git commit -m "${commit}" -q

# Download NPM Packages
echo 'Installing NPM Packages'
npm install

# Prepare assets
gulp init
