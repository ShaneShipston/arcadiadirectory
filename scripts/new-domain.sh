#!/usr/bin/env bash
for ((i=1;i<=$#;i++))
do
    if [ ${!i} = "--site" ];
    then ((i++))
        site=${!i};

    elif [ ${!i} = "--tld" ];
    then ((i++))
        tld=${!i};
    fi
done

if [ -z ${tld+x} ]; then
    tld="vm";
fi

reboot_webserver_helper() {
    sudo service apache2 restart

    echo 'Rebooting your webserver'
}

bash /var/www/scripts/generate-ssl.sh ${site}.${tld}

# Change to relevant directory
cd /var/www/sites

# Create MySQL database
mysqladmin -uroot -proot create ${site}

# Create folder
mkdir ${site}

# Add virtual host
MY_WEB_CONFIG="<VirtualHost *:80>
    ServerName ${site}.${tld}
    DocumentRoot /var/www/sites/${site}
    <Directory \"/var/www/sites/${site}\">
        Options Indexes FollowSymLinks
        AllowOverride all
        Require all granted
    </Directory>
</VirtualHost>

<VirtualHost *:443>
    ServerName ${site}.${tld}
    DocumentRoot /var/www/sites/${site}
    <Directory \"/var/www/sites/${site}\">
        Options Indexes FollowSymLinks
        AllowOverride all
        Require all granted
    </Directory>

    SSLEngine on
    SSLCertificateFile /home/vagrant/certs/${site}.${tld}.crt
    SSLCertificateKeyFile /home/vagrant/certs/${site}.${tld}.key
</VirtualHost>"
sudo echo "$MY_WEB_CONFIG" | sudo tee /etc/apache2/sites-available/${site}.conf

sudo a2ensite ${site}
sudo service apache2 reload

# Restart web server
reboot_webserver_helper
