#!/usr/bin/env
if [ "$#" -ne 1 ]
then
  echo "Usage: Must supply a version"
  exit 1
fi

oldversion=$(php -v | head -n 1 | cut -d " " -f 2 | cut -f1-2 -d".")
version=$1

sudo a2enmod php${version}
sudo a2dismod php${oldversion}
sudo service apache2 restart

sudo update-alternatives --set php /usr/bin/php${version}
sudo update-alternatives --set phar /usr/bin/phar${version}
sudo update-alternatives --set phar.phar /usr/bin/phar.phar${version}
sudo update-alternatives --set phpize /usr/bin/phpize${version}
sudo update-alternatives --set php-config /usr/bin/php-config${version}
