#!/usr/bin/env bash

if [ "$#" -ne 1 ]
then
  echo "Usage: Must supply a domain"
  exit 1
fi

domain=$1

cd /home/vagrant/certs

openssl genrsa -out ${domain}.key 2048
openssl req -new -key ${domain}.key -out ${domain}.csr -subj "/C=CA/O=Arcadia/OU=Arcadia/CN=${domain}"

cat > ${domain}.ext << EOF
authorityKeyIdentifier=keyid,issuer
basicConstraints=CA:FALSE
keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
subjectAltName = @alt_names
[alt_names]
DNS.1 = ${domain}
EOF

openssl x509 -req -in ${domain}.csr -CA myCA.pem -CAkey myCA.key -CAcreateserial -out ${domain}.crt -days 180 -sha256 -extfile ${domain}.ext -passin file:passphrase.txt
