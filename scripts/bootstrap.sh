#!/usr/bin/env bash
for ((i=1;i<=$#;i++))
do
    if [ ${!i} = "--directory" ];
    then ((i++))
        directory=${!i};

    elif [ ${!i} = "--script" ];
    then ((i++))
        script=${!i};
    fi
done

# Move to proper directory
cd ${directory}

# Run script
ssh vagrant@192.168.33.10 -oStrictHostKeyChecking=no "bash /var/www/scripts/${script} $@"
