#!/usr/bin/env bash
for ((i=1;i<=$#;i++))
do
    if [ ${!i} = "--directory" ];
    then ((i++))
        directory=${!i};
    fi
done

# Move up a directory
cd ${directory}

# start server
vagrant up
