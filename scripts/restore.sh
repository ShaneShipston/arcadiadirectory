#!/usr/bin/env bash
for ((i=1;i<=$#;i++))
do
    if [ ${!i} = "--site" ];
    then ((i++))
        site=${!i};

    elif [ ${!i} = "--output" ];
    then ((i++))
        output=${!i};

    elif [ ${!i} = "--type" ];
    then ((i++))
        type=${!i};
    fi
done

cd /var/www/backups/${site}

if [ $type = "full" ]; then
    cp ${output}-full.zip /var/www/sites/${output}-full.zip
    cd /var/www/sites
    rm -rf /var/www/sites/${site}
    unzip ${output}-full.zip >/dev/null

    # Database
    mysqladmin -uroot -proot drop ${site} -f
    mysqladmin -uroot -proot create ${site}
    mysql -uroot -proot ${site} < ${site}/db.sql

    # Clean up
    rm -rf ${output}-full.zip
    rm -rf ${site}/db.sql
elif [ $type = "files" ]; then
    cp ${output}-files.zip /var/www/sites/${output}-files.zip
    cd /var/www/sites
    rm -rf /var/www/sites/${site}
    unzip ${output}-files.zip >/dev/null

    # Clean up
    rm -rf ${output}-files.zip
elif [ $type = "db" ]; then
    mysqladmin -uroot -proot drop ${site} -f
    mysqladmin -uroot -proot create ${site}
    mysql -uroot -proot ${site} < ${output}-db.sql
fi
