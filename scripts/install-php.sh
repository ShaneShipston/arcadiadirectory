#!/usr/bin/env
if [ "$#" -ne 1 ]
then
  echo "Usage: Must supply a version"
  exit 1
fi

version=$1

sudo add-apt-repository ppa:ondrej/php -y
sudo apt update

sudo apt install php${version} php${version}-bcmath php${version}-bz2 php${version}-cgi php${version}-cli php${version}-common php${version}-curl php${version}-dev php${version}-enchant php${version}-gd php${version}-imap php${version}-intl php${version}-ldap php${version}-mbstring php${version}-mysql php${version}-odbc php${version}-opcache php${version}-pspell php${version}-readline php${version}-tidy php${version}-xml php${version}-zip libapache2-mod-php${version} -y

PHP_USER_INI_PATH=/etc/php/${version}/apache2/conf.d/user.ini

sudo echo 'display_startup_errors = On' | sudo tee -a $PHP_USER_INI_PATH
sudo echo 'display_errors = On' | sudo tee -a $PHP_USER_INI_PATH
sudo echo 'error_reporting = E_ALL' | sudo tee -a $PHP_USER_INI_PATH
sudo echo 'upload_max_filesize = 64M' | sudo tee -a $PHP_USER_INI_PATH
sudo echo 'post_max_size = 64M' | sudo tee -a $PHP_USER_INI_PATH
sudo echo 'opache.enable = 0' | sudo tee -a $PHP_USER_INI_PATH

sudo sed -i s,\;opcache.enable=0,opcache.enable=0,g /etc/php/${version}/apache2/php.ini

sudo service apache2 restart

bash /var/www/scripts/change-php.sh ${version}
