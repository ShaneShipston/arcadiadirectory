#!/usr/bin/env bash
backupdb=1
backupfiles=1

for ((i=1;i<=$#;i++))
do
    if [ ${!i} = "--site" ];
    then ((i++))
        site=${!i};

    elif [ ${!i} = "--output" ];
    then ((i++))
        output=${!i};

    elif [ ${!i} = "--db-only" ]; then
        backupfiles=0

    elif [ ${!i} = "--files-only" ]; then
        backupdb=0
    fi
done

mkdir -p /var/www/backups/${site}

if [ $backupdb -eq 1 ] && [ $backupfiles -eq 1 ]; then
    # echo "Full Backup"
    cd /var/www/sites/${site}
    mysqldump -uroot -proot --databases ${site} > db.sql
    cd /var/www/sites
    zip -r ${output}-full.zip ${site} -x "**/node_modules/*" >/dev/null
    mv ${output}-full.zip /var/www/backups/${site}/${output}-full.zip
    rm -rf /var/www/sites/${site}/db.sql
elif [ $backupdb -eq 1 ]; then
    # echo "DB Only"
    cd /var/www/backups/${site}
    mysqldump -uroot -proot --databases ${site} > ${output}-db.sql
elif [ $backupfiles -eq 1 ]; then
    # echo "Files Only"
    cd /var/www/sites
    zip -r ${output}-files.zip ${site} -x "**/node_modules/*" >/dev/null
    mv ${output}-files.zip /var/www/backups/${site}/${output}-files.zip
fi
