#!/usr/bin/env bash

# Install jq
apt-get install -y jq zip unzip

# Change into root directory
cd /var/www

# Start with a clean folder
rm -rf public

# Create folders
mkdir sites
mkdir config
mkdir backups

# Fix Line Endings
find scripts -type f -print0 | xargs -0 dos2unix

# SSL Prep
cd /home/vagrant

mkdir certs
cd certs

# Generate a random password
openssl rand -base64 48 > passphrase.txt

# Creating Private Key
openssl genrsa -passout file:passphrase.txt -des3 -out myCA.key 2048

# Creating Root Key
openssl req -x509 -new -nodes -key myCA.key -sha256 -days 36500 -out myCA.pem -passin file:passphrase.txt -subj "/C=CA/O=Arcadia/OU=Arcadia/CN=Arcadia"

# Change Permissions
chown vagrant:vagrant passphrase.txt
chown vagrant:vagrant myCA.key
chown vagrant:vagrant myCA.pem

cd ..

chown vagrant:vagrant certs

# Make accessible
cp /home/vagrant/certs/myCA.pem /var/www/arcadia.pem
