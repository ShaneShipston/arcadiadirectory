# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|

    config.vm.box = "shaneshipston/arcadiabox"
    config.vm.box_version = ">= 1.0.0"
    config.vm.hostname = "arcadiabox"
    config.vm.box_check_update = false
    config.vm.network "forwarded_port", guest: 80, host: 8080
    config.vm.network "private_network", ip: "192.168.33.10"

    if Vagrant::Util::Platform.windows? then
        config.vm.synced_folder ".", "/var/www", :mount_options => ["dmode=777", "fmode=666"]
    else
        config.vm.synced_folder ".", "/var/www", type: 'nfs', mount_options: ['actimeo=1', 'nolock']
    end

    config.ssh.forward_agent = true

    config.vm.provider "virtualbox" do |vb|
        vb.customize ["modifyvm", :id, "--memory", "2048"]
        vb.customize ["modifyvm", :id, "--cpus", "1"]
        vb.customize ["modifyvm", :id, "--natdnsproxy1", "on"]
        vb.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
    end

    # Configure The Public Key For SSH Access
    if File.exists? File.expand_path("~/.ssh/id_rsa.pub")
        config.vm.provision "shell" do |s|
            s.inline = "echo $1 | grep -xq \"$1\" /home/vagrant/.ssh/authorized_keys || echo \"\n$1\" | tee -a /home/vagrant/.ssh/authorized_keys"
            s.args = [File.read(File.expand_path("~/.ssh/id_rsa.pub"))]
        end
    end

    # Copy The SSH Private Keys To The Box
    config.vm.provision "shell" do |s|
        s.privileged = false
        s.inline = "echo \"$1\" > /home/vagrant/.ssh/$2 && chmod 600 /home/vagrant/.ssh/$2"
        s.args = [File.read(File.expand_path("~/.ssh/id_rsa")), "~/.ssh/id_rsa".split('/').last]
    end

    config.vm.provision "shell", path: "scripts/server-setup.sh"

end
